# Codeless Flappy Bird
A recreation of Flappy Bird made in Godot 3.4.4, but without using any code or scripting of any kind.
The whole game is run by default nodes, and signals carefully connecting them.

Meant as a fun challenge - The engine wasn't made with this kind of workflow in mind, so figuring out how to do some of the mechanics was tricky.

I wonder how much more is possible? I might try another game in the future.

You can play the game on the browser, or download a 64-bit Windows version, on [the Itch page](https://l4vo5.itch.io/godot-codeless-flappy-bird).

You can find the source code on [GitLab](https://gitlab.com/L4Vo5/codeless-flappy-bird).